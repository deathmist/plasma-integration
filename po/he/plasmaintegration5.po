# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasmaintegration5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-11 00:39+0000\n"
"PO-Revision-Date: 2023-10-03 09:56+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.1\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:260
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "פתיחת קובץ"

#: platformtheme/kdeplatformfiledialoghelper.cpp:261
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "שמירת קובץ"

#: platformtheme/kdeplatformtheme.cpp:401
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "שמירה של הכול"

#: platformtheme/kdeplatformtheme.cpp:405
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr "&כן"

#: platformtheme/kdeplatformtheme.cpp:407
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "כן להכול"

#: platformtheme/kdeplatformtheme.cpp:409
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr "&לא"

#: platformtheme/kdeplatformtheme.cpp:411
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "לא להכול"

#: platformtheme/kdeplatformtheme.cpp:414
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "ביטול"

#: platformtheme/kdeplatformtheme.cpp:416
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "לנסות שוב"

#: platformtheme/kdeplatformtheme.cpp:418
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "התעלמות"

#: platformtheme/kdirselectdialog.cpp:122
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "תיקייה חדשה"

#: platformtheme/kdirselectdialog.cpp:128
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "תיקייה חדשה"

#: platformtheme/kdirselectdialog.cpp:129
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"יצירת תיקייה חדשה תחת:\n"
"%1"

#: platformtheme/kdirselectdialog.cpp:160
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "קובץ או תיקייה בשם %1 כבר קיימים."

#: platformtheme/kdirselectdialog.cpp:169
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "אין לך הרשאות ליצירת תיקייה זו."

#: platformtheme/kdirselectdialog.cpp:271
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "בחירת תיקייה"

#: platformtheme/kdirselectdialog.cpp:279
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "תיקייה חדשה…"

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "תיקייה חדשה…"

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "העברה לאשפה"

#: platformtheme/kdirselectdialog.cpp:354
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "מחיקה"

#: platformtheme/kdirselectdialog.cpp:365
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "הצגת תיקיות מוסתרות"

#: platformtheme/kdirselectdialog.cpp:372
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "מאפיינים"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "הצגת תיקיות מוסתרות"
